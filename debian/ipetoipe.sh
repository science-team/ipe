#! /bin/sh

set -e

IPELATEXDIR=/tmp/ipetoipe-$$
trap "rm -rf $IPELATEXDIR" 0 1 15

export IPELATEXDIR
xIPELIBDIRx/ipetoipe-bin ${1+"$@"}
